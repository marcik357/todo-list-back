import { MongoClient, ObjectId, ServerApiVersion } from 'mongodb';
const uri = "mongodb+srv://marcik357:Corsair499331916@marcik357db.io1o0xi.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(uri, {
    serverApi: {
        version: ServerApiVersion.v1,
        strict: true,
        deprecationErrors: true,
    }
});

export async function getAllTodos() {
    try {
        await client.connect();
        const db = await client.db("todos");
        const todosCollection = await db.collection("todoList");
        const result = await todosCollection.find({}).toArray();
        return result;
    } finally {
        await client.close();
    }
}

export async function addTodo(todo) {
    try {
        await client.connect();
        const db = await client.db("todos");
        const todosCollection = await db.collection("todoList");
        const newTodo = await todosCollection.insertOne(todo);
        const result = await todosCollection.findOne({ _id: newTodo.insertedId })
        return result
    } finally {
        await client.close();
    }
}

export async function changeTodoStatus(id, todo) {
    try {
        await client.connect();
        const db = await client.db("todos");
        const todosCollection = await db.collection("todoList");
        const editedTodo = await todosCollection.updateOne({ _id: new ObjectId(id) }, { $set: {status: todo.status} });
        const result = await todosCollection.findOne({ _id: editedTodo.insertedId })
        return result
    } finally {
        await client.close();
    }
}

export async function deleteTodo(id) {
    try {
        await client.connect();
        const db = await client.db("todos");
        const todosCollection = await db.collection("todoList");
        const result = await todosCollection.deleteOne({ _id: new ObjectId(id) });
        return result
    } finally {
        await client.close();
    }
}


// export async function run() {
//     try {
//         // const todo = {
//         //     text: 'text from todo alone',
//         //     status: 'undone'
//         // }

//         // Connect the client to the server	(optional starting in v4.7)
//         await client.connect();
//         // Send a ping to confirm a successful connection
//         // await client.db("admin").command({ ping: 1 });
//         const db = await client.db('todos')
//         const todosCollection = await db.collection('todoList')


//         // await todosCollection.insertOne(todo)
//         // await todosCollection.insertMany(todo)

//         // const result = await todosCollection.findOne({status:"undone"})
//         // const result = await todosCollection.find({ status: "undone" }).toArray()
//         const result = await todosCollection.find({}).toArray()
//         return result

//         // await todosCollection.updateOne({ text: 'text from todo 2' }, {$set: { text: 'new text from todo 2' }})
//         // await todosCollection.updateMany({ status: 'undone' }, {$set: { extratext: 'new text from todo extra' }})
//         // await todosCollection.deleteOne({ text: "text from todo alone" })
//         // await todosCollection.deleteMany({ status: "done" })

//         console.log("Pinged your deployment. You successfully connected to MongoDB!");
//     } finally {
//         // Ensures that the client will close when you finish/error
//         await client.close();
//     }
// }
// // run().catch(console.dir);
