import express from 'express'
import cors from 'cors'
import { addTodo, deleteTodo, changeTodoStatus, getAllTodos } from './db/index.js'

const app = express()
app.use(cors())
app.use(express.json())


app.get('/', async function (req,res) {
    res.send('hello from backend')
})

app.get("/todos", async function (req, res) {
    const data = await getAllTodos(); 
    res.send(JSON.stringify(data));
});

app.post("/todos", async function (req, res) {
    const data = await addTodo(req.body);
    res.send(data);
});

app.put("/todos/:id", async function (req, res) {
    const todoId = req.params.id
    const updatedTodo = req.body
    const data = await changeTodoStatus(todoId, updatedTodo);
    res.send(data);
});

app.delete("/todos/:id", async function (req, res) {
    const todoId = req.params.id
    const data = await deleteTodo(todoId);
    res.send(data);
})


app.listen(3001, () => {
    console.log('Server started')
})